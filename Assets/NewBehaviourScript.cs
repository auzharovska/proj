using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.forward);
            transform.localScale /= 2;
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.back);
            transform.localScale /= 2;
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left);
            transform.localScale /= 2;
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right);
            transform.localScale /= 2;
    }   }
     private void OnTriggerEnter(Collider other)
    { 
        transform.localScale = new Vector3(10f, 10f, 10f);
    }
}     